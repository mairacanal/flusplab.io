---
title:  "Flusp Site"
image:  "flusp-site"
ref: "flusp-site"
categories: "projects.md"
id: 6
---

# Description

FLUSP’s website

# Recommendations

* HTML
* CSS
* JavaScript
* Jekyll

# URLs

* [FLUSP site](https://gitlab.com/flusp/site)
