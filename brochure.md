---
title: "KernelDevDay - Sponsor"
lang: en
ref: kerneldevday
layout: default
---

{% include add_multilang.html %}

KernelDevDay is a computer programming event organized by FLUSP (FLOSS at USP)
in which participating teams spend a certain contiguous period (approximately
10 hours) working on contributions to some subsystem of the Linux Kernel. For
this first edition of KernelDevDay, we want to contribute to drivers from the
Industrial Input/Output (IIO) that are not yet ready to be merged into the main
Linux kernel tree due to technical reasons.

We have a simple and clear mission by promoting this event: we want to expand
the Kernel Linux community in Brazil. Given FLUSP's experience in contributing
to various free software communities, we believe that we can act as a catalyst
in this process of attracting new developers. We are also convinced that
KernelDevDay could have an impact in the society by contributing to the
training of our students and aid raising the reputation of the Brazilian
community in the international scenario. With these goals in mind, and aiming
to provide an excellent experience for participants, we are looking for
sponsors to support us in this event. Our main priority is to collect resources
for investing in food, printing flyers/banners/badges, and production of gifts
for participants.  We offer the following packages:

{% include items_with_modals.html %}

Transparency is embedded at the root of FLUSP values. For this reason, we
emphasize that any exceeding funding received by the organization of the event
will be reverted for the following purposes:

* Purchase of tables of the folding type, to be used by extension groups of IME-USP;
* Financing the tickets of some FLUSP members for Debconf 2019;
* Making FLUSP t-shirts for group members and guests;

Finally, we will disclose the amount we can raise and all the expenses we had
in the form of a report at the end of the event. Sponsoring this event is
investing in the formation of new free software developers in Brazil and
encouraging future professionals to build a more collaborative society.
